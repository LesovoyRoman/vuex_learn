import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        count: 0,
    },
    mutations: {
        countPlus(stateCount){
            stateCount.count++;
        }
    },
    actions: {
        countPlus(stateCount) {
            stateCount.commit('countPlus')
        }
    }
});